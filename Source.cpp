#include <iostream>
#include "Helpers.h"

int main()
{
	int a, b;
	std::cout << "Enter a:\n";
	std::cin >> a;
	
	std::cout << "Enter b:\n";
	std::cin >> b;

	int result = SumSquared(a, b);
	std::cout << "Result = " << result << std::endl;

}